

Redmine::Plugin.register :helpdesk_notification do
  name 'Helpdesk Notification plugin'
  author 'Anonym'
  description 'This plugin send notification for helpdesk users'
  version '0.0.1'
  settings default: {
    helpdesk_domain: 'http://localhost:3000',
  }, partial: 'settings/helpdesk_settings'
end

# create custom field
ProjectCustomField.find_or_create_by name: 'helpdesk domain', field_format: 'link', editable: true, visible: true

Rails.configuration.to_prepare do
  require 'issue'
  require 'journal'
  unless Issue.included_modules.include? IssuePatch
    Issue.send(:include, IssuePatch)
  end

  unless Journal.included_modules.include? JournalPatch
    Journal.send(:include, JournalPatch)
  end
end
