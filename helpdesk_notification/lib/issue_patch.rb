require 'issue'

module IssuePatch
  def self.included(base)
    base.send(:include, InstanceMethods)
    base.class_eval do
      unloadable
      has_one :support_request
      # alias_method :old_notified_users, :notified_users 
      # alias_method :notified_users, :notified_users_helpdesk 
    end
  end

  module InstanceMethods
    def notified_users_helpdesk
      notified_users = old_notified_users
      # TODO: add check changes.
      # notified_users << support_request.email if support_request.present?
      notified_users
    end
  end
end

