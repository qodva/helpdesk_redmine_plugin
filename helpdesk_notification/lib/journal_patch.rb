require 'journal'

module JournalPatch
  def self.included(base)
    base.send(:include, InstanceMethods)
    base.class_eval do
      unloadable
      after_commit :notify_request_user
    end
  end

  module InstanceMethods
    def notify_request_user
      return if self.private_notes

      issue = self.issue
      return unless issue.present?
      
      support_request = issue.support_request
      return unless support_request.present? 

      status_changes = self.details.where(prop_key: :status_id).first
      return unless self.notes || status_changes.present? 

      project_helpdesk_domain = self.project.custom_field_values.find { |cfv| cfv.custom_field.name == 'helpdesk domain'}
      
      if project_helpdesk_domain.present? && project_helpdesk_domain.value.present? 
        helpdesk_domain = project_helpdesk_domain.value
      else
        helpdesk_domain = Setting['plugin_helpdesk_notification'][:helpdesk_domain]
      end

      Thread.new do
        RestClient.post("#{helpdesk_domain}/notifications/#{support_request.id}/changed",
          {journal: self.id}
        )
      end
    
    end
  end
end

